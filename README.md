# hackathon

Acesso ao AWS (usuário ubuntu)
------------------------------
ssh -i hackathon.pem ubuntu@18.223.197.218

Usuário root
------------
Usuário: root
Senha: 123ejaBM

MySQL
-----
Usuário: root
Senha: 123ejaBM

Usuário: hackathon
Senha: 123ejaBM
Banco: hackathon
IP: 18.223.197.218

Conectar ao MySQL
-----------------
mysql -u hackathon -p -h 18.223.197.218 -P 3306 -D hackathon


create user 'hackathon'@'%' identified by '123ejaBM';
grant all on hackathon.* to 'hackathon'@'%' identified by '123ejaBM';

Instalar Apache e PHP
---------------------
https://www.vultr.com/docs/how-to-install-apache-mysql-and-php-on-ubuntu-16-04

Endereços
---------
http://18.223.197.218
https://bit.ly/2NAN30h

Assinatura Digital
------------------

Chave privada:
openssl genrsa -out chave_privada.pem 2048

Certificate Signing Request:
openssl req -new -sha256 -key chave_privada.pem -out csr.pem

Certificado:
openssl x509 -req -in csr.pem -signkey chave_privada.pem -out chave_publica.pem

https://developer.android.com/training/articles/keystore?hl=pt-br
