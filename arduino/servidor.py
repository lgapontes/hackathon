# -*- coding: utf-8 -*-
from flask import Flask

app = Flask(__name__)

# Apenas para testes locais
#from flask_cors import CORS
#CORS(app)

@app.route('/arduino/<codigo>')
def arduino(codigo):
    print('codigo = ' + codigo)

if __name__ == "__main__":
    app.run(port=8080,host='18.223.197.218')
