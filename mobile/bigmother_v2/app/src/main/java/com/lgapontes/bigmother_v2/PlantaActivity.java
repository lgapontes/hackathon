package com.lgapontes.bigmother_v2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lgapontes.bigmother_v2.infraestrutura.HttpUtils;
import com.lgapontes.bigmother_v2.infraestrutura.Localizador;
import com.lgapontes.bigmother_v2.modelo.PT;
import com.lgapontes.bigmother_v2.modelo.Pessoa;
import com.lgapontes.bigmother_v2.modelo.RTI;
import com.lgapontes.bigmother_v2.modelo.Tag;
import com.lgapontes.bigmother_v2.service.RTIService;
import com.lgapontes.bigmother_v2.service.TagService;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PlantaActivity extends AppCompatActivity {

    private final int LOCATION_PERMISSION_REQUEST_CODE = 1252;

    private RelativeLayout layoutPlanta;
    private RelativeLayout plantaLoading;
    private ImageView imagemPosicao;
    private TextView textPosicao;
    private TextView textData;

    private boolean rtisCarregadas;
    private boolean tagsCarregadas;

    private Button buttonCancelar;
    private Button buttonVisualizar;
    private Button buttonAprovar;
    private Button buttonAprovarBloqueado;
    private Button buttonQuitar;
    private ImageView plantaProducao;

    private Map<Integer,ImageView> imagensPT;

    public static PT ptSelecionada;

    private void verificarPermissao() {
        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= 23) { // Marshmallow
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
            } else {
                iniciarGPS();
            }
        } else {
            iniciarGPS();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                iniciarGPS();
            }
        }
    }

    private void iniciarGPS() {
        new Localizador(this,plantaProducao,imagemPosicao,textPosicao,textData);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planta);
        this.layoutPlanta = (RelativeLayout) findViewById(R.id.layout_planta);
        this.imagemPosicao = (ImageView) findViewById(R.id.imagem_posicao);
        this.plantaLoading = (RelativeLayout) findViewById(R.id.planta_loading);
        this.textPosicao = (TextView) findViewById(R.id.planta_latlog);
        this.textData = (TextView) findViewById(R.id.planta_data);
        this.plantaProducao = (ImageView) findViewById(R.id.planta_producao);

        this.imagensPT = new HashMap<Integer,ImageView>();

        verificarPermissao();

        this.buttonCancelar = (Button) findViewById(R.id.botao_cancelar);
        this.buttonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assinar(ptSelecionada,AssinaturaActivity.CANCELAR);
            }
        });

        this.buttonVisualizar = (Button) findViewById(R.id.botao_visualizar);
        this.buttonVisualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://18.223.197.218/tema/pdf/pt.pdf";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        this.buttonAprovar = (Button) findViewById(R.id.botao_aprovar);
        this.buttonAprovar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assinar(ptSelecionada,AssinaturaActivity.APROVAR);
            }
        });

        this.buttonAprovarBloqueado = (Button) findViewById(R.id.botao_aprovar_bloqueado);

        this.buttonQuitar = (Button) findViewById(R.id.botao_quitar);
        this.buttonQuitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assinar(ptSelecionada,AssinaturaActivity.QUITAR);
            }
        });

        HttpUtils.get("http://18.223.197.218/index.php/api/tags", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONArray array) {
                try{
                    List<Tag> tags = TagService.getInstance().obterTags(PlantaActivity.this,array);
                    for (int i=0; i<tags.size(); i++) {
                        inflarTAG(tags.get(i));
                    }
                } catch (Exception e) {
                    Toast.makeText(PlantaActivity.this,"Erro ao obter as TAGs!",Toast.LENGTH_LONG);
                }
                tagsCarregadas = true;
                verificarCarregamento();
            }
        });
        HttpUtils.get("http://18.223.197.218/index.php/api/rtis", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONArray array) {
                try{
                    List<RTI> rtis = RTIService.getInstance().obterRTIs(PlantaActivity.this,array);
                    for (int i=0; i<rtis.size(); i++) {
                        inflarRTI(rtis.get(i));
                    }
                } catch (Exception e) {
                    Toast.makeText(PlantaActivity.this,"Erro ao obter as RTIs!",Toast.LENGTH_LONG);
                }
                rtisCarregadas = true;
                verificarCarregamento();
            }
        });

    }

    public void verificarCarregamento() {
        if (rtisCarregadas && tagsCarregadas) {
            plantaLoading.setVisibility(View.GONE);
            layoutPlanta.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        ptSelecionada = (PT) getIntent().getSerializableExtra("pt");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(ptSelecionada.getDescricao());

        if (this.imagensPT.containsKey(ptSelecionada.getCodigo())) {
            this.imagensPT.get(ptSelecionada.getCodigo()).setVisibility(View.VISIBLE);
        } else {
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(R.drawable.ic_pt);
            imageView.setPadding(ptSelecionada.getX1(),ptSelecionada.getY1(),0,0);
            this.imagensPT.put(ptSelecionada.getCodigo(),imageView);
            this.layoutPlanta.addView(imageView);
        }

        if (ptSelecionada.getStatus().getCodigo() == 2) {
            buttonCancelar.setVisibility(View.VISIBLE);
            buttonAprovar.setVisibility(View.GONE);
            buttonAprovarBloqueado.setVisibility(View.VISIBLE);
            buttonQuitar.setVisibility(View.GONE);
        } else if (ptSelecionada.getStatus().getCodigo() == 1) {
            buttonCancelar.setVisibility(View.VISIBLE);
            buttonAprovar.setVisibility(View.GONE);
            buttonAprovarBloqueado.setVisibility(View.GONE);
            buttonQuitar.setVisibility(View.VISIBLE);
        } else {
            buttonCancelar.setVisibility(View.GONE);
            buttonAprovar.setVisibility(View.GONE);
            buttonAprovarBloqueado.setVisibility(View.GONE);
            buttonQuitar.setVisibility(View.GONE);
        }

        super.onResume();
    }

    @Override
    protected void onStop() {
        Iterator<ImageView> imagens = this.imagensPT.values().iterator();
        while (imagens.hasNext()) {
            ImageView imagem = imagens.next();
            imagem.setVisibility(View.GONE);
        }
        super.onStop();
    }

    public void inflarTAG(Tag tag) {
        ImageView imageView = new ImageView(this);
        imageView.setImageResource( tag.isInibida() ? R.drawable.ic_tag_by : R.drawable.ic_tag_ok);
        imageView.setPadding(tag.getPaddingX(),tag.getPaddingY(),0,0);

        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://18.223.197.218/tema/pdf/tag.pdf";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        this.layoutPlanta.addView(imageView);
    }

    public void inflarRTI(RTI rti) {
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.ic_rti);
        imageView.setPadding(rti.getPaddingX(),rti.getPaddingY(),0,0);

        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://18.223.197.218/tema/pdf/rti.pdf";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        this.layoutPlanta.addView(imageView);
    }

    public void assinar(PT pt, int acao) {
        Intent intent = new Intent(this,AssinaturaActivity.class);
        intent.putExtra("pt",pt);
        intent.putExtra("acao",new Integer(acao));
        startActivity(intent);
    }

}
