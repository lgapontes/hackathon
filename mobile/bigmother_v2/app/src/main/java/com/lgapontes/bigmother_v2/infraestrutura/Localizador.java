package com.lgapontes.bigmother_v2.infraestrutura;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.lgapontes.bigmother_v2.MainActivity;
import com.lgapontes.bigmother_v2.PlantaActivity;
import com.lgapontes.bigmother_v2.R;
import com.lgapontes.bigmother_v2.modelo.PT;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

// https://github.com/codepath/android_guides/wiki/Retrieving-Location-with-LocationServices-API
public class Localizador  implements GoogleApiClient.ConnectionCallbacks {

    private long INTERVALO = 1000;
    private float METROS = 0.5f;
    private GoogleApiClient api;
    private LocationRequest mLocationRequest;
    private Activity activity;
    private FusedLocationProviderClient client;

    private ImageView planta;
    private ImageView imagemPosicao;
    private TextView textPosicao;
    private TextView textData;

    private Button botao;
    private Button botaoBloqueado;

    public Localizador(Activity activity, ImageView planta, ImageView imagemPosicao, TextView textPosicao, TextView textData) {
        this.activity = activity;
        this.planta = planta;
        this.imagemPosicao = imagemPosicao;
        this.textPosicao = textPosicao;
        this.textData = textData;

        this.api = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        api.connect();

        Log.e(MainActivity.TAG_LOG,"GPS iniciado!");
    }

    @SuppressLint({"MissingPermission", "RestrictedApi"})
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(MainActivity.TAG_LOG,"Conectando GPS...");

        mLocationRequest = new LocationRequest();
        mLocationRequest.setSmallestDisplacement(METROS);
        mLocationRequest.setInterval(INTERVALO);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(this.activity);
        settingsClient.checkLocationSettings(locationSettingsRequest);
        this.client = LocationServices.getFusedLocationProviderClient(this.activity);

        final Location base = new Location("base");
        //base.setLatitude(-22.8512042d);
        //base.setLongitude(-43.2320771d);

        base.setLatitude(-22.8525488d);
        base.setLongitude(-43.2316694d);

        final Location topo = new Location("topo");
        //topo.setLatitude(-22.8511089d);
        //topo.setLongitude(-43.2321051d);

        topo.setLatitude(-22.8524901d);
        topo.setLongitude(-43.2317977d);

        final float distanciaGeral = base.distanceTo(topo);

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        final int alturaPlanta = Math.round(height * 0.8f);
        final int meioPlanta = Math.round(width / 2) - 64;

        client.requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        try {
                            if (botao == null) botao = (Button) activity.findViewById(R.id.botao_aprovar);
                            if (botaoBloqueado == null) botaoBloqueado = (Button) activity.findViewById(R.id.botao_aprovar_bloqueado);

                            Log.e(MainActivity.TAG_LOG,"Executando GPS...");


                            if ( (locationResult != null) && (locationResult.getLastLocation() != null) ) {

                                Log.e(MainActivity.TAG_LOG,"Posicao GPS...");
                                
                                Location location = locationResult.getLastLocation();

                                String text =
                                        "(" + Double.toString( location.getLatitude() ) + "," +
                                                Double.toString( location.getLongitude() ) + ")";
                                String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
                                textPosicao.setText(text + "  - " + timeStamp);

                                float distanciaAtual = base.distanceTo(location);
                                float percentual = distanciaAtual / distanciaGeral;
                                int top = alturaPlanta - Math.round(alturaPlanta * percentual);
                                if ( (top < 0) || (top > alturaPlanta) ) {
                                    top = Math.round( alturaPlanta / 2 ) - 500;
                                }
                                imagemPosicao.setVisibility(View.VISIBLE);
                                imagemPosicao.setPadding(meioPlanta,top,0,0);

                                String log = "%=" + percentual;
                                if (PlantaActivity.ptSelecionada != null) {
                                    log += " y=" + PlantaActivity.ptSelecionada.getY1();

                                    if (((top+64) >= PlantaActivity.ptSelecionada.getY1()) && ((top-64) <= (PlantaActivity.ptSelecionada.getY1() + 206)) ) {
                                        log += " >> ÁREA";

                                        if (PlantaActivity.ptSelecionada.getStatus().getCodigo() == 2) {
                                            if (botao != null) botao.setVisibility(View.VISIBLE);
                                            if (botaoBloqueado != null) botaoBloqueado.setVisibility(View.GONE);
                                        }
                                    } else {
                                        log += " >> FORA";

                                        if (PlantaActivity.ptSelecionada.getStatus().getCodigo() == 2) {
                                            if (botao != null) botao.setVisibility(View.GONE);
                                            if (botaoBloqueado != null) botaoBloqueado.setVisibility(View.VISIBLE);
                                        }
                                    }
                                } else {
                                    log += " >> NULL";
                                }

                                textData.setText(log);

                            }

                        } catch (Exception e) {
                            Toast.makeText(Localizador.this.activity, "Erro GPS", Toast.LENGTH_LONG);
                            textData.setText("ERROR: " + e.getMessage());
                        }
                    }
                },
                Looper.myLooper()
        );
    }

    @Override
    public void onConnectionSuspended(int i) {}
}
