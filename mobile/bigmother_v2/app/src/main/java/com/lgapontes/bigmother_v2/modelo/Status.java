package com.lgapontes.bigmother_v2.modelo;

import java.io.Serializable;

public class Status implements Serializable {

    private int codigo;
    private String descricao;

    public Status(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }
}
