package com.lgapontes.bigmother_v2.modelo;

public class Tag {

    private int codigo;
    private String descricao;
    private int paddingX;
    private int paddingY;
    private boolean inibida;

    public Tag(int codigo, String descricao, int paddingX, int paddingY, boolean inibida) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.paddingX = paddingX;
        this.paddingY = paddingY;
        this.inibida = inibida;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public int getPaddingX() {
        return paddingX;
    }

    public int getPaddingY() {
        return paddingY;
    }

    public boolean isInibida() {
        return inibida;
    }
}
