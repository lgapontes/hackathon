package com.lgapontes.bigmother_v2.service;

import android.content.Context;
import android.widget.Toast;

import com.lgapontes.bigmother_v2.MainActivity;
import com.lgapontes.bigmother_v2.modelo.PT;
import com.lgapontes.bigmother_v2.modelo.Pessoa;
import com.lgapontes.bigmother_v2.modelo.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PTService {

    private static PTService service = new PTService();
    private PTService() {}

    public static PTService getInstance() { return service; }

    public List<PT> obterPTs(Context context, JSONArray array) {
        List<PT> lista = new ArrayList<PT>();

            try {
                for (int i=0; i<array.length();i++) {
                    JSONObject dados = array.getJSONObject(i);

                    PT pt = new PT();
                    pt.setCodigo(Integer.parseInt(dados.getString("codigo")));
                    pt.setPlataforma(dados.getString("plataforma"));
                    pt.setAno(Integer.parseInt(dados.getString("ano")));
                    pt.setDescricao(dados.getString("descricao"));
                    pt.setAssinatura(dados.getString("assinatura"));
                    pt.setX1(Integer.parseInt(dados.getString("x1")));
                    pt.setX2(Integer.parseInt(dados.getString("x2")));
                    pt.setY1(Integer.parseInt(dados.getString("y1")));
                    pt.setY2(Integer.parseInt(dados.getString("y2")));
                    pt.setData(dados.getString("data"));

                    Pessoa pessoa = new Pessoa(
                            Integer.parseInt(dados.getString("codigo_pessoa")),
                            dados.getString("pessoa")
                    );
                    pt.setPessoa(pessoa);

                    Status status = new Status(
                            Integer.parseInt(dados.getString("codigo_status")),
                            dados.getString("status")
                    );
                    pt.setStatus(status);

                    if (pt.getStatus().getCodigo() <= 2) {
                        lista.add(pt);
                    }
                }

            } catch (Exception e) {
                Toast.makeText(context,"Erro ao converter as PTs!", Toast.LENGTH_SHORT);
            }

         return lista;
    }

}
