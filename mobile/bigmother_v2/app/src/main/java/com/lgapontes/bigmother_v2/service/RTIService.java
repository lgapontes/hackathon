package com.lgapontes.bigmother_v2.service;

import android.content.Context;
import android.widget.Toast;

import com.lgapontes.bigmother_v2.modelo.RTI;
import com.lgapontes.bigmother_v2.modelo.Tag;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RTIService {

    private static RTIService service = new RTIService();
    private RTIService() {}

    public static RTIService getInstance() { return service; }


    public List<RTI> obterRTIs(Context context, JSONArray array) {
        List<RTI> lista = new ArrayList<RTI>();

        try {
            for (int i=0; i<array.length();i++) {
                JSONObject dados = array.getJSONObject(i);

                RTI rti = new RTI(
                        Integer.parseInt(dados.getString("codigo")),
                        Integer.parseInt(dados.getString("paddingx")),
                        Integer.parseInt(dados.getString("paddingy"))
                );

                lista.add(rti);
            }

        } catch (Exception e) {
            Toast.makeText(context,"Erro ao converter o RTIs!", Toast.LENGTH_SHORT);
        }

        return lista;
    }

}
