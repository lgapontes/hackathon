package com.lgapontes.bigmother_v2.service;

import android.content.Context;
import android.widget.Toast;

import com.lgapontes.bigmother_v2.modelo.Tag;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TagService {

    private static TagService service = new TagService();
    private TagService() {}

    public static TagService getInstance() { return service; }

    public List<Tag> obterTags(Context context, JSONArray array) {
        List<Tag> lista = new ArrayList<Tag>();

        try {
            for (int i=0; i<array.length();i++) {
                JSONObject dados = array.getJSONObject(i);

                Tag tag = new Tag(
                        Integer.parseInt(dados.getString("codigo")),
                        dados.getString("descricao"),
                        Integer.parseInt(dados.getString("paddingx")),
                        Integer.parseInt(dados.getString("paddingy")),
                        Integer.parseInt(dados.getString("bypass")) == 0 ? false : true
                );

                lista.add(tag);
            }

        } catch (Exception e) {
            Toast.makeText(context,"Erro ao converter o TAGs!", Toast.LENGTH_SHORT);
        }

        return lista;
    }

}
