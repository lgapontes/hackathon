package com.lgapontes.bigmother_v2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lgapontes.bigmother_v2.infraestrutura.EnCryptorV2;
import com.lgapontes.bigmother_v2.infraestrutura.HttpUtils;
import com.lgapontes.bigmother_v2.modelo.PT;
import com.lgapontes.bigmother_v2.modelo.Status;
import com.lgapontes.bigmother_v2.modelo.Tag;
import com.lgapontes.bigmother_v2.service.TagService;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class AssinaturaActivity extends AppCompatActivity {

    private static final String ALIAS = "BIG_MOTHER";

    private TextView assinaturaNome;
    private TextView assinaturaHora;
    private EditText assinaturaDigital;

    public static int APROVAR = 0;
    public static int CANCELAR = 1;
    public static int QUITAR = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assinatura);
        assinaturaNome = (TextView) findViewById(R.id.assinatura_nome);
        assinaturaHora = (TextView) findViewById(R.id.assinatura_hora);
        assinaturaDigital = (EditText) findViewById(R.id.assinatura_digital);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setTitle("Assinatura Digital");
    }

    @Override
    protected void onResume() {
        PT pt = (PT) getIntent().getSerializableExtra("pt");
        int acao = ( (Integer) getIntent().getSerializableExtra("acao")).intValue();
        assinaturaNome.setText(pt.getPessoa().getNome());
        assinaturaHora.setText(pt.getData());
        String assinatura = encryptText(pt.toString());

        try {

            JSONObject json = new JSONObject();
            json.put("codigo",Integer.toString(pt.getCodigo()));
            String timeStamp = new SimpleDateFormat("DD-MM-YYYY HH:mm:ss").format(new Date());
            json.put("data",timeStamp);
            json.put("assinatura", assinatura);

            PlantaActivity.ptSelecionada.setAssinatura(assinatura);
            PlantaActivity.ptSelecionada.setData(timeStamp);

            if (acao == APROVAR) {
                PlantaActivity.ptSelecionada.setStatus(new Status(1,"Aprovada"));

                HttpUtils.post(this,"http://18.223.197.218/index.php/api/pts_aprovar", json, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONArray array) {
                        Toast.makeText(AssinaturaActivity.this,"PT liberada!",Toast.LENGTH_LONG);
                    }
                });
            }
            if (acao == QUITAR) {
                PlantaActivity.ptSelecionada.setStatus(new Status(4,"Quitada"));

                HttpUtils.post(this,"http://18.223.197.218/index.php/api/pts_quitar", json, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONArray array) {
                        Toast.makeText(AssinaturaActivity.this,"PT quitada!",Toast.LENGTH_LONG);
                    }
                });
            }
            if (acao == CANCELAR) {
                PlantaActivity.ptSelecionada.setStatus(new Status(3,"Cancelada"));

                HttpUtils.post(this,"http://18.223.197.218/index.php/api/pts_cancelar", json, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONArray array) {
                        Toast.makeText(AssinaturaActivity.this,"PT cancelada!",Toast.LENGTH_LONG);
                    }
                });
            }

        } catch (Exception e) {
            Log.e(MainActivity.TAG_LOG, e.getMessage(), e);
        }

        super.onResume();
    }

    private String encryptText(String pt) {
        try {
            String assinatura = EnCryptorV2.assinar(ALIAS,pt.toString());
            assinaturaDigital.setText(assinatura);
            return assinatura;
        } catch (Exception e) {
            Log.e(MainActivity.TAG_LOG, e.getMessage(), e);
        }
        return "";
    }


    public void voltar(View view) {
        finish();
    }
}
