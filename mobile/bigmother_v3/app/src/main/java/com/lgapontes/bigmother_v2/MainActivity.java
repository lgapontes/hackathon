package com.lgapontes.bigmother_v2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.lgapontes.bigmother_v2.infraestrutura.HttpUtils;
import com.lgapontes.bigmother_v2.modelo.PT;
import com.lgapontes.bigmother_v2.service.PTService;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static String TAG_LOG = "LOG_BIG_MOTHER";
    private ListView listaPTs;
    private RelativeLayout listaLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.listaPTs = (ListView) findViewById(R.id.lista_pts);
        this.listaLoading = (RelativeLayout) findViewById(R.id.lista_loading);

        this.listaPTs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> lista, View item, int posicao, long id) {
                PT pt = (PT) lista.getItemAtPosition(posicao);
                Intent intent = new Intent(MainActivity.this,PlantaActivity.class);
                intent.putExtra("pt",pt);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        this.listaPTs.setVisibility(View.GONE);
        this.listaLoading.setVisibility(View.VISIBLE);

        HttpUtils.get("http://18.223.197.218/index.php/api/pts", null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONArray array) {
                try{
                    List<PT> lista = PTService.getInstance().obterPTs(MainActivity.this,array);
                    ArrayAdapter<PT> adapter = new ArrayAdapter<PT>(MainActivity.this,android.R.layout.simple_list_item_1,lista);
                    MainActivity.this.listaPTs.setAdapter(adapter);

                    MainActivity.this.listaLoading.setVisibility(View.GONE);
                    MainActivity.this.listaPTs.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this,"Erro ao obter as PTs!",Toast.LENGTH_LONG);
                }
            }

        });

        super.onResume();
    }
}
