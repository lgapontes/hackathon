package com.lgapontes.bigmother_v2.modelo;

import java.io.Serializable;

public class Pessoa implements Serializable {

    private int codigo;
    private String nome;

    public Pessoa(int codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNome() {
        return nome;
    }
}
