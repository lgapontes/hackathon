package com.lgapontes.bigmother_v2.modelo;

public class RTI {

    private int codigo;
    private int paddingX;
    private int paddingY;

    public RTI(int codigo, int paddingX, int paddingY) {
        this.codigo = codigo;
        this.paddingX = paddingX;
        this.paddingY = paddingY;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getPaddingX() {
        return paddingX;
    }

    public int getPaddingY() {
        return paddingY;
    }
}
