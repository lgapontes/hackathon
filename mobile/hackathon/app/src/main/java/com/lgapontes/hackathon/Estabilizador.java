package com.lgapontes.hackathon;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

public class Estabilizador {

    private int TOTAL_PARA_ESTABILIZAR = 3;
    private int TOTAL_PARA_LIMPAR = 3;
    private float METROS = 2f;

    private List<Location> valores;
    private boolean estaEstabilizado;
    private int contarParaLimpar;


    public Estabilizador() {
        this.valores = new ArrayList<Location>();
        this.estaEstabilizado = false;
        this.contarParaLimpar = 0;
    }

    public void push(Location valor) {
        if (valor != null) {
            this.valores.add(valor);
        }
    }

    public boolean estaEstabilizado() {
        if (this.valores.size() >= TOTAL_PARA_ESTABILIZAR) {

            if (estaEstabilizado) {
                contarParaLimpar++;

                if (contarParaLimpar == TOTAL_PARA_LIMPAR) {
                    this.valores = new ArrayList<Location>();
                    this.estaEstabilizado = false;
                    this.contarParaLimpar = 0;
                    return false;
                }

                return true;
            } else {
                int qtde = this.valores.size();
                Location ultimo = this.valores.get(qtde-1);
                Location penultimo = this.valores.get(qtde-2);
                Location antipenultimo = this.valores.get(qtde-3);

                float metros1 = ultimo.distanceTo(penultimo);
                float metros2 = ultimo.distanceTo(antipenultimo);
                if ( ( Float.compare(metros1,METROS) <= 0 ) && ( Float.compare(metros2, METROS) <= 0 ) ) {
                    this.estaEstabilizado = true;
                    return true;
                }
            }
        }
        return false;
    }

    /*
    private List<Location> valoresMedios(List<Location> lista) {

        int qtde = lista.size();

        double totalLat = 0d;
        double totalLog = 0d;
        for (Location l : lista) {
            totalLat += l.getLatitude();
            totalLog += l.getLongitude();
        }
        double mediaLatitude = totalLat / qtde;
        double mediaLongitude = totalLog / qtde;

        double[] tempLatitude = new double[qtde];
        double[] tempLongitude = new double[qtde];

        for (int i=0; i<qtde; i++) {
            tempLatitude[i] = Math.pow((lista.get(i).getLatitude() - mediaLatitude),2d);
            tempLongitude[i] = Math.pow((lista.get(i).getLongitude() - mediaLongitude),2d);
        }

        double medianaLatitude = 0d;
        double medianaLongitude = 0d;
        for (int i=0; i<qtde; i++) {
            medianaLatitude += tempLatitude[i];
            medianaLongitude += tempLongitude[i];
        }
        medianaLatitude = medianaLatitude / (qtde-1);
        medianaLongitude = medianaLongitude / (qtde-1);

        List<Location> eleitos = new ArrayList<Location>();
        for (Location eleito : lista) {
            double absLatitude = Math.abs(eleito.getLatitude());
            double absLongitude = Math.abs(eleito.getLongitude());

            if ( (absLatitude <= medianaLatitude) && (absLongitude <= medianaLongitude)) {
                eleitos.add(eleito);
            }
        }

        return eleitos;
    }

    public Location obterValorEstabilizado() {
        this.iniciado = false;

        List<Location> eleitos1 = valoresMedios(this.valores);
        List<Location> eleitos2 = valoresMedios(eleitos1);
        if (eleitos2.size() > 0) {
            return eleitos2.get(0);
        } else if (eleitos1.size() > 0) {
            return eleitos1.get(0);
        } else {
            return this.valores.get( this.valores.size() - 1 );
        }
    }
    */

}
