package com.lgapontes.hackathon;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.SimpleDateFormat;
import java.util.Date;

// https://github.com/codepath/android_guides/wiki/Retrieving-Location-with-LocationServices-API
public class Localizador  implements GoogleApiClient.ConnectionCallbacks {

    private long INTERVALO = 1000;
    private float METROS = 1f;
    private GoogleApiClient api;
    private LocationRequest mLocationRequest;
    private Activity activity;
    private FusedLocationProviderClient client;

    private TextView textViewPosicaoAtual;
    private TextView textViewLogData;

    private EditText editTextPosicaoA;
    private EditText editTextPosicaoB;
    private EditText editTextPosicaoC;

    private RelativeLayout relativeLayoutPlanta;
    private ImageView imagemPlanta;
    private ImageView imagemPosicao;

    private Estabilizador estabilizador;

    private ProgressBar progressBar;
    private TextView textViewMark;

    private ImageButton botaoPontoA;
    private ImageButton botaoPontoB;
    private ImageButton botaoPontoC;

    public Localizador(Activity activity) {

        textViewPosicaoAtual = (TextView) activity.findViewById(R.id.location);
        textViewLogData = (TextView) activity.findViewById(R.id.logdata);

        editTextPosicaoA = (EditText) activity.findViewById(R.id.pontoA);
        editTextPosicaoB = (EditText) activity.findViewById(R.id.pontoB);
        editTextPosicaoC = (EditText) activity.findViewById(R.id.pontoC);

        relativeLayoutPlanta = (RelativeLayout) activity.findViewById(R.id.relativeLayoutPlanta);
        imagemPlanta = (ImageView) activity.findViewById(R.id.imagemPlanta);
        imagemPosicao = (ImageView) (ImageView) activity.findViewById(R.id.imagemPosicao);

        this.estabilizador = new Estabilizador();

        botaoPontoA = (ImageButton) activity.findViewById(R.id.botaoPontoA);
        botaoPontoB = (ImageButton) activity.findViewById(R.id.botaoPontoB);
        botaoPontoC = (ImageButton) activity.findViewById(R.id.botaoPontoC);
        progressBar = (ProgressBar) activity.findViewById(R.id.progressBar);
        textViewMark = (TextView) activity.findViewById(R.id.mark);

        this.activity = activity;
        this.api = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        api.connect();
    }

    @SuppressLint({"MissingPermission", "RestrictedApi"})
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setSmallestDisplacement(METROS);
        mLocationRequest.setInterval(INTERVALO);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(this.activity);
        settingsClient.checkLocationSettings(locationSettingsRequest);
        this.client = LocationServices.getFusedLocationProviderClient(this.activity);

        client.requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        /*
                            -22.4025592
                            -41.8057928
                        */
                        try {

                            if ( (locationResult != null) && (locationResult.getLastLocation() != null) ) {

                                if (!Mapa.getInstance().tamanhoFoiDefinido()) {
                                    Mapa.getInstance().definirTamanhoImagem(
                                            imagemPlanta.getWidth(),
                                            imagemPlanta.getHeight(),
                                            imagemPosicao.getWidth(),
                                            imagemPosicao.getHeight()
                                    );
                                }

                                Location location = locationResult.getLastLocation();
                                double lat = location.getLatitude();
                                double lon = location.getLongitude();

                                String text = "(" + Double.toString( lat ) + "," + Double.toString( lon ) + ")";
                                String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
                                textViewPosicaoAtual.setText(text + " - " + timeStamp);

                                Mapa.getInstance().setLocation(location);
                                estabilizador.push(location);

                                if (estabilizador.estaEstabilizado()) {
                                    progressBar.setVisibility(View.GONE);
                                    textViewMark.setVisibility(View.VISIBLE);
                                } else {
                                    progressBar.setVisibility(View.VISIBLE);
                                    textViewMark.setVisibility(View.GONE);
                                }

                                int[] posicao = Mapa.getInstance().calcularPosicao();

                                if (Mapa.getInstance().podeExibirPosicao()) {
                                    imagemPosicao.setPadding(posicao[0],posicao[1],0,0);
                                    imagemPosicao.setVisibility(View.VISIBLE);
                                } else {
                                    imagemPosicao.setVisibility(View.INVISIBLE);
                                }

                                textViewLogData.setText( Mapa.getInstance().getLog() );
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                Looper.myLooper()
        );
    }

    public void looper() {
        Mapa.getInstance().zerar();
        editTextPosicaoA.setText("");
        editTextPosicaoB.setText("");
        editTextPosicaoC.setText("");
    }

    public void capturarPontoA() {
        Location location = Mapa.getInstance().definirA();
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        String text = "(" + Double.toString( lat ) + "," + Double.toString( lon ) + ")";
        editTextPosicaoA.setText(text);
    }
    public void capturarPontoB() {
        Location location = Mapa.getInstance().definirB();
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        String text = "(" + Double.toString( lat ) + "," + Double.toString( lon ) + ")";
        editTextPosicaoB.setText(text);
    }
    public void capturarPontoC() {
        Location location = Mapa.getInstance().definirC();
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        String text = "(" + Double.toString( lat ) + "," + Double.toString( lon ) + ")";
        editTextPosicaoC.setText(text);
    }

    @Override
    public void onConnectionSuspended(int i) {}
}
