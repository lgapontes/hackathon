package com.lgapontes.hackathon;

import android.location.Location;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;

public class Mapa {

    private static Mapa instance = new Mapa();
    public static Mapa getInstance() { return instance; }
    private Mapa() { this.tamanhoDefinido = false; }
    private Location location;

    public void zerar() {
        locationA = null;
        locationB = null;
        locationC = null;
    }

    private Location locationA;
    private Location locationB;
    private Location locationC;

    private int plantWidth;
    private int plantHeight;
    private int iconWidth;
    private int iconHeight;
    private boolean tamanhoDefinido;

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location definirA() {
        this.locationA = location;
        return location;
    }

    public Location definirB() {
        this.locationB = location;
        return location;
    }

    public Location definirC() {
        this.locationC = location;
        return location;
    }

    public void definirTamanhoImagem(int pw, int ph, int iw, int ih) {
        this.plantWidth = pw;
        this.plantHeight = ph;
        this.iconWidth = iw;
        this.iconHeight = ih;
        this.tamanhoDefinido = true;
    }

    public boolean tamanhoFoiDefinido() { return this.tamanhoDefinido; }

    private float ab;
    private float bc;

    private float za;
    private float zb;
    private float zc;

    private double x;
    private double y;
    private double paddingLeft;
    private double paddingTop;

    private double percentualX;
    private double percentualY;

    private String round(double value) {
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(value);
    }

    public String getLog() {
        return "ab=" + round(ab) + " " +
                "bc=" + round(bc) + " " +
                "%x=" + round(percentualX) + " " +
                "%y" + round(percentualY) + " " +
                "pl=" + round(paddingLeft) + " " +
                "pt=" + round(paddingTop);
    }

    public boolean podeExibirPosicao() {
        return ( (this.locationA != null) && (this.locationB != null) && (this.locationC != null) );
    }

    private double calcularArea(double a, double b, double c) {
        return (a + b + c) / 2;
    }

    private double calcularAltura(double base, double area) {
        return (2 * area) / base;
    }

    public int[] calcularPosicao() {
        int[] retorno = new int[]{ 0 , 0 };

        try {

            if ( (this.locationA == null) || (this.locationB == null) ) {
                return retorno;
            }

            ab = this.locationA.distanceTo(this.locationB);

            if (this.locationC == null) {
                return retorno;
            }

            bc = this.locationB.distanceTo(this.locationC);

            za = this.locationA.distanceTo(this.location);
            zb = this.locationB.distanceTo(this.location);
            zc = this.locationC.distanceTo(this.location);

            double areaA = calcularArea(ab, zb, za);
            y = calcularAltura(ab, areaA);

            double areaB = calcularArea(bc, zc, zb);
            x = calcularAltura(bc, areaB);

            percentualX = x / ab;
            percentualY = y / bc;

            double widthDisponivel = plantWidth - iconWidth;
            double heightDisponivel = plantHeight - iconHeight;

            paddingLeft = widthDisponivel - Math.round( percentualX * widthDisponivel );
            paddingTop = Math.round( percentualY * heightDisponivel );

            if (Double.compare(paddingLeft,0) < 0) paddingLeft = 0d;
            if (Double.compare(paddingLeft,widthDisponivel) > 0) paddingLeft = widthDisponivel;
            if (Double.compare(paddingTop,0) < 0) paddingTop = 0d;
            if (Double.compare(paddingTop,heightDisponivel) > 0) paddingTop = heightDisponivel;

            retorno[0] = new Double(paddingLeft).intValue();
            retorno[1] = new Double(paddingTop).intValue();

        } catch (Exception e) {}

        return retorno;
    }

}
