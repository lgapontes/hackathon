package com.saxena.kshitij.miband2connect;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONObject;

import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.entity.mime.Header;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

public class HttpUtils {

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(url, params, responseHandler);
    }

    public static void post(Context context, String url, JSONObject jsonParams, AsyncHttpResponseHandler responseHandler) {
        try {
            //StringEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
            ByteArrayEntity entity = new ByteArrayEntity(jsonParams.toString().getBytes("UTF-8"));
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            client.post(context, url, entity, "application/json", responseHandler);
        } catch (Exception e) {
            Log.e("ERRO","Erro no post " + e.getMessage());
        }
    }

    /*
    public void post(Context context, String url, JSONObject jsonParams, ResponseHandlerInterface responseHandler) {
        client.post(this,url,jsonParams);
    }
    */
}

