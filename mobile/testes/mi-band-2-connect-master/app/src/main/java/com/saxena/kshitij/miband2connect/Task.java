package com.saxena.kshitij.miband2connect;

import android.os.AsyncTask;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

public class Task extends AsyncTask<Object,Object,Object> {

    @Override
    protected Object doInBackground(Object... objects) {

        return null;
    }

    public boolean estaZicada(JSONArray array) {
        try {
            for (int i=0; i<array.length();i++) {
                JSONObject dados = array.getJSONObject(i);

                if (dados.getString("zicada").equals("1")) {
                    return true;
                }
            }

            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
